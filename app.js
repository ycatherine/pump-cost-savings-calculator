$(function() {

    /**
     * Initialise CodaSlider.
     */
    $('#slider-id').codaSlider({
        firstPanelToLoad: 1,
        autoHeight: true,
        dynamicTabs: true,
        panelTitleSelector: "h1.title",
        dynamicArrowLeftText: "<",
        dynamicArrowRightText: ">",
        slideEaseFunction: "easeOutCirc",
        slideEaseDuration: 700,
        autoHeightEaseFunction: "easeOutCirc",
        autoHeightEaseDuration: 700,
        continuous: false
    });

    /**
     * Min Slider
     */
    $( ".slider-min" ).slider({
        animate: true,
        range: "min",
        value: 5089,
        min: 2220,
        max: 10545,
        step: 25,

        //this gets a live reading of the value and prints it on the page
        slide: function( event, ui ) {
            $( "#slider-result-min" ).html( ui.value );
        },

        //this updates the hidden form field so we can submit the data using a form
        change: function(event, ui) {

            if ( ui.value > $( ".slider-max" ).slider( "value" ) ) {

                ui.value = $( ".slider-max" ).slider( "value" );
                $( ".slider-min" ).slider( "value", ui.value );

            }

            $('#slider-result-min').html(ui.value);
            $('#hidden-min').attr('value', ui.value);
            cost_saving_calculator.update();
        }

    });

    /**
     * Max Slider
     */
    $( ".slider-max" ).slider({
        animate: true,
        range: "min",
        value: 10545,
        min: 2220,
        max: 10545,
        step: 25,


        //this gets a live reading of the value and prints it on the page
        slide: function( event, ui ) {
            $( "#slider-result-max" ).html( ui.value );
        },

        //this updates the hidden form field so we can submit the data using a form
        change: function(event, ui) {
            $('#hidden-max').attr('value', ui.value);

            if ( ui.value < $( ".slider-min" ).slider( "value" ) ) {

                ui.value = $( ".slider-min" ).slider( "value" );
                $( ".slider-max" ).slider( "value", ui.value );

            }

            $('#slider-result-max').html(ui.value);
            $('#hidden-max').attr('value', ui.value);
            cost_saving_calculator.update();

        }
    });

});
