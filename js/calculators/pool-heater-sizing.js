/**
*	Pool Heater Sizing calculator.
*	Unfortunately the Excel document this was ported from has some magic numbers (i.e. their 
*	purpose or meaning was unknown), so they have been copied as is.
*	@author Chris Botman <chris@botman.com.au> on behalf of Wordplay Media
*/

var pool_heater_sizing_calculator = {

	init : function() {
	
		// init validation and triggers
		$( '#pool-heater-sizing' ).validate();
		$( 'input, select' ).change( function() {
			pool_heater_sizing_calculator.update();
		});
		
		// all fields are preset, so perform first calculation
		pool_heater_sizing_calculator.update();
	},
	
	update : function () {
	
		this.toggle_heating_costs();
		this.calculate_mj_requirements();
		this.calculate_recommended_heater();
		this.calculate_costs();
		this.calculate_ave_temp_rise();
		this.graph_temperature();
		this.graph_initial_mj_requirement();
		this.graph_heat_up_cost();
		this.graph_maintenance_cost();
	},
	
	toggle_heating_costs : function() {
	
		if ( $( '#gas_type' ).val() == 'natural' ) {
			$( '.natural-only' ).show();
			$( '.lpg-only' ).hide();
		}
		else { // lpg
			$( '.natural-only' ).hide();
			$( '.lpg-only' ).show();
		}
	},
	
	locations : {
		// These are monthly average temperatures, jan to dec
		"Adelaide" : [23.1, 22.6, 20.4, 17.6, 13.7, 11.4, 10.4, 11.3, 13.8, 16.8, 19.1, 20.8],
		"Brisbane" : [25.6, 25.3, 23.8, 21.5, 17.6, 14.8, 13.5, 15.4, 18.8, 21.9, 23.9, 25.1],
		"Melbourne" : [20.1, 19.8, 17.6, 14.6, 11.2, 9, 8.2, 9.5, 11.9, 14.6, 16.5, 18.5],
		"Perth" : [23.9, 23.4, 21.2, 17.9, 15.2, 13.3, 11.9, 12.6, 14.5, 17.3, 20, 22.7],
		"Sydney" : [22.9, 23, 21.4, 18.4, 14.3, 12, 10.9, 12.5, 15.4, 18.6, 20.7, 22.1],
		"FNQ Coastal" : [25.6, 25.3, 23.8, 23.5, 20.6, 17.8, 16.5, 18.4, 21.8, 24.9, 25.9, 27.1]
	},
	
	calculate_mj_requirements : function() {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var total_meters = $( '#average_length' ).val() * $( '#average_width' ).val() * 
			$( '#average_depth' ).val();
		var desired_temp = $( '#desired_temp' ).val();
		var location = this.locations[$( '#location' ).val()];
		
		function get_mj_required( average_temp, is_covered ) {
			var mj_required = (
				total_meters
				* 1.16 // Magic number...
				* 1.3 // Magic number...
				* ( desired_temp - average_temp )
				* 4
			);
			
			if ( is_covered ) {
				mj_required *= 0.885;
			}
			
			return Math.round( mj_required );
		}
		
		$( '#mj_required_jan' ).html( get_mj_required( location[0], false ) );
		$( '#mj_required_feb' ).html( get_mj_required( location[1], false ) );
		$( '#mj_required_mar' ).html( get_mj_required( location[2], false ) );
		$( '#mj_required_apr' ).html( get_mj_required( location[3], false ) );
		$( '#mj_required_may' ).html( get_mj_required( location[4], false ) );
		$( '#mj_required_jun' ).html( get_mj_required( location[5], false ) );
		$( '#mj_required_jul' ).html( get_mj_required( location[6], false ) );
		$( '#mj_required_aug' ).html( get_mj_required( location[7], false ) );
		$( '#mj_required_sep' ).html( get_mj_required( location[8], false ) );
		$( '#mj_required_oct' ).html( get_mj_required( location[9], false ) );
		$( '#mj_required_nov' ).html( get_mj_required( location[10], false ) );
		$( '#mj_required_dec' ).html( get_mj_required( location[11], false ) );
		
		$( '#mj_required_jan_cover' ).html( get_mj_required( location[0], true ) );
		$( '#mj_required_feb_cover' ).html( get_mj_required( location[1], true ) );
		$( '#mj_required_mar_cover' ).html( get_mj_required( location[2], true ) );
		$( '#mj_required_apr_cover' ).html( get_mj_required( location[3], true ) );
		$( '#mj_required_may_cover' ).html( get_mj_required( location[4], true ) );
		$( '#mj_required_jun_cover' ).html( get_mj_required( location[5], true ) );
		$( '#mj_required_jul_cover' ).html( get_mj_required( location[6], true ) );
		$( '#mj_required_aug_cover' ).html( get_mj_required( location[7], true ) );
		$( '#mj_required_sep_cover' ).html( get_mj_required( location[8], true ) );
		$( '#mj_required_oct_cover' ).html( get_mj_required( location[9], true ) );
		$( '#mj_required_nov_cover' ).html( get_mj_required( location[10], true ) );
		$( '#mj_required_dec_cover' ).html( get_mj_required( location[11], true ) );
	},

	heaters : { 
		//  model 	// mj input	
		"HX 70" : 63,
		"HX 120" : 108,
		"JX 130" : 117,
		"MX 150" : 135,
		"JX 160" : 144,
		"MX 250" : 211.5,
		"MX 300" : 261,
		"MX 400" : 324,
		"HR 600" : 540,
		"HR 800" : 720,
		"HR 1000" : 900,
		"HR 1200" : 1080,
		"HR 1400" : 1260,
		"HR 1600" : 1440,
		"HR 1800" : 1620,
		"Viron 250"	: 190,
		"Viron 350" : 290,
		"Viron 450"	: 400,
		"Viron 550"	: 490
	},
	
	calculate_recommended_heater : function() {
		
		// check required fields
		if ( $( '.step2 input' ).hasClass( 'error' ) ) {
			return;
		}

		var mj_required_pool_temp = $( '#mj_required_jul' ).html() / $( '#desired_heat_up_hours' ).val();

		var recommended_pool_heaters = this.get_recommended_heaters( mj_required_pool_temp );
		$( '#recommended_pool_heater' ).html( recommended_pool_heaters.heater_names );
		$( '#recommended_conventional_output' ).val( recommended_pool_heaters.conventional_heater_output );
		$( '#recommended_viron_output' ).val( recommended_pool_heaters.viron_heater_output );
	},
	
	// Used by calculate_recommended_heater
	get_recommended_heaters : function( mj_required ) {
	
		var conventional_heater = '';
		var conventional_heater_output = 0;
		
		// get pump type
		if ( this.heaters['HX 70'] > mj_required ) {
			conventional_heater = 'HX 70';
		}
		else if ( this.heaters['HX 120'] > mj_required ) {
			conventional_heater = 'HX 120';
		}
		else if ( this.heaters['JX 130'] > mj_required ) {
			conventional_heater = 'JX 130';
		}
		else if ( this.heaters['MX 150'] > mj_required ) {
			conventional_heater = 'MX 150';
		}
		else if ( this.heaters['JX 160'] > mj_required ) {
			conventional_heater = 'JX 160';
		}
		else if ( this.heaters['MX 250'] > mj_required ) {
			conventional_heater = 'MX 250';
		}
		else if ( this.heaters['MX 300'] > mj_required ) {
			conventional_heater = 'MX 300';
		}
		else if ( this.heaters['MX 400'] > mj_required ) {
			conventional_heater = 'MX 400';
		}
		else if ( this.heaters['HR 600'] > mj_required ) {
			conventional_heater = 'HR 600';
		}
		else if ( this.heaters['HR 800'] > mj_required ) {
			conventional_heater = 'HR 800';
		}
		else if ( this.heaters['HR 1000'] > mj_required ) {
			conventional_heater = 'HR 1000';
		}
		else if ( this.heaters['HR 1200'] > mj_required ) {
			conventional_heater = 'HR 1200';
		}
		else if ( this.heaters['HR 1400'] > mj_required ) {
			conventional_heater = 'HR 1400';
		}
		else if ( this.heaters['HR 1600'] > mj_required ) {
			conventional_heater = 'HR 1600';
		}
		else if ( this.heaters['HR 1800'] > mj_required ) {
			conventional_heater = 'HR 1800';
		}
		
		var viron_heater = '';
		var viron_heater_output = 0;
		
		if ( this.heaters['Viron 250'] > mj_required ) {
			viron_heater = 'Viron 250';
		}
		else if ( this.heaters['Viron 350'] > mj_required ) {
			viron_heater = 'Viron 350';
		}
		else if ( this.heaters['Viron 450'] > mj_required ) {
			viron_heater = 'Viron 450';
		}
		else if ( this.heaters['Viron 550'] > mj_required ) {
			viron_heater = 'Viron 550';
		}
		
		var heater_names = '';
		
		if ( conventional_heater == '' && viron_heater == '' ) {
		
			name = 'Contact your local AstralPool office';
		}
		else {
		
			// get outputs
			if ( viron_heater != '' ) {
				heater_names += viron_heater;
				viron_heater_output = this.heaters['' + viron_heater];
			}
			if ( conventional_heater != '' ) {
				
				if ( viron_heater != '' ) {
					heater_names += ' or ';
				}
				heater_names += conventional_heater;
				conventional_heater_output = this.heaters['' + conventional_heater];
			}
		}
		
		return {
			'heater_names': heater_names, 
			'conventional_heater_output': conventional_heater_output,
			'viron_heater_output': viron_heater_output
		};
	},
	
	calculate_costs : function() {
	
		// check required fields
		if ( $( '.step2 input, .step6 input' ).hasClass( 'error' ) ) {
			return;
		}

		//
		// pool_heat_up_cost
		
		function get_heat_cost( mj_required ) {

			var gas_type = $( '#gas_type' ).val();
			var heat_cost = 0;
		
			if ( gas_type == 'natural' ) {
			
				var natural_cost_mj = $( '#natural_cost_mj' ).val();
				heat_cost = (
					mj_required
					* natural_cost_mj
					/100
				);
			}
			else {
				
				var lpg_cost_litre = $( '#lpg_cost_litre' ).val();
				heat_cost = (
					mj_required
					* lpg_cost_litre
					/ 25
				);
			}

			return heat_cost.toFixed( 2 );
		}
		
		$( '#heat_cost_jan' ).html( get_heat_cost( $( '#mj_required_jan' ).html() ) );
		$( '#heat_cost_feb' ).html( get_heat_cost( $( '#mj_required_feb' ).html() ) );
		$( '#heat_cost_mar' ).html( get_heat_cost( $( '#mj_required_mar' ).html() ) );
		$( '#heat_cost_apr' ).html( get_heat_cost( $( '#mj_required_apr' ).html() ) );
		$( '#heat_cost_may' ).html( get_heat_cost( $( '#mj_required_may' ).html() ) );
		$( '#heat_cost_jun' ).html( get_heat_cost( $( '#mj_required_jun' ).html() ) );
		$( '#heat_cost_jul' ).html( get_heat_cost( $( '#mj_required_jul' ).html() ) );
		$( '#heat_cost_aug' ).html( get_heat_cost( $( '#mj_required_aug' ).html() ) );
		$( '#heat_cost_sep' ).html( get_heat_cost( $( '#mj_required_sep' ).html() ) );
		$( '#heat_cost_oct' ).html( get_heat_cost( $( '#mj_required_oct' ).html() ) );
		$( '#heat_cost_nov' ).html( get_heat_cost( $( '#mj_required_nov' ).html() ) );
		$( '#heat_cost_dec' ).html( get_heat_cost( $( '#mj_required_dec' ).html() ) );
		
		$( '#heat_cost_jan_cover' ).html( get_heat_cost( $( '#mj_required_jan_cover' ).html() ) );
		$( '#heat_cost_feb_cover' ).html( get_heat_cost( $( '#mj_required_feb_cover' ).html() ) );
		$( '#heat_cost_mar_cover' ).html( get_heat_cost( $( '#mj_required_mar_cover' ).html() ) );
		$( '#heat_cost_apr_cover' ).html( get_heat_cost( $( '#mj_required_apr_cover' ).html() ) );
		$( '#heat_cost_may_cover' ).html( get_heat_cost( $( '#mj_required_may_cover' ).html() ) );
		$( '#heat_cost_jun_cover' ).html( get_heat_cost( $( '#mj_required_jun_cover' ).html() ) );
		$( '#heat_cost_jul_cover' ).html( get_heat_cost( $( '#mj_required_jul_cover' ).html() ) );
		$( '#heat_cost_aug_cover' ).html( get_heat_cost( $( '#mj_required_aug_cover' ).html() ) );
		$( '#heat_cost_sep_cover' ).html( get_heat_cost( $( '#mj_required_sep_cover' ).html() ) );
		$( '#heat_cost_oct_cover' ).html( get_heat_cost( $( '#mj_required_oct_cover' ).html() ) );
		$( '#heat_cost_nov_cover' ).html( get_heat_cost( $( '#mj_required_nov_cover' ).html() ) );
		$( '#heat_cost_dec_cover' ).html( get_heat_cost( $( '#mj_required_dec_cover' ).html() ) );
		
		//
		// pool_daily_cost
		
		$( '#daily_cost_jan' ).html( ( $( '#heat_cost_jan' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_feb' ).html( ( $( '#heat_cost_feb' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_mar' ).html( ( $( '#heat_cost_mar' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_apr' ).html( ( $( '#heat_cost_apr' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_may' ).html( ( $( '#heat_cost_may' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_jun' ).html( ( $( '#heat_cost_jun' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_jul' ).html( ( $( '#heat_cost_jul' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_aug' ).html( ( $( '#heat_cost_aug' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_sep' ).html( ( $( '#heat_cost_sep' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_oct' ).html( ( $( '#heat_cost_oct' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_nov' ).html( ( $( '#heat_cost_nov' ).html() * 0.3 ).toFixed( 2 ) );
		$( '#daily_cost_dec' ).html( ( $( '#heat_cost_dec' ).html() * 0.3 ).toFixed( 2 ) );

		// For the covered daily costs, we reduce the uncovered daily costs
		$( '#daily_cost_jan_cover' ).html( ( $( '#daily_cost_jan' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_feb_cover' ).html( ( $( '#daily_cost_feb' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_mar_cover' ).html( ( $( '#daily_cost_mar' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_apr_cover' ).html( ( $( '#daily_cost_apr' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_may_cover' ).html( ( $( '#daily_cost_may' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_jun_cover' ).html( ( $( '#daily_cost_jun' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_jul_cover' ).html( ( $( '#daily_cost_jul' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_aug_cover' ).html( ( $( '#daily_cost_aug' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_sep_cover' ).html( ( $( '#daily_cost_sep' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_oct_cover' ).html( ( $( '#daily_cost_oct' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_nov_cover' ).html( ( $( '#daily_cost_nov' ).html() * 0.6 ).toFixed( 2 ) );
		$( '#daily_cost_dec_cover' ).html( ( $( '#daily_cost_dec' ).html() * 0.6 ).toFixed( 2 ) );
		
		//
		// holiday costs
		// the * 1 is to cast to a number
		
		var holiday_cost = 0;
		
		holiday_cost = ( $( '#heat_cost_apr' ).html() * 1 ) + ( $( '#daily_cost_apr' ).html() * 12 );
		$( '#holiday_cost_apr' ).html( holiday_cost.toFixed( 2 ) );
		holiday_cost = ( $( '#heat_cost_jun' ).html() * 1 ) + ( $( '#daily_cost_jun' ).html() * 12 );
		$( '#holiday_cost_jun' ).html( holiday_cost.toFixed( 2 ) );
		holiday_cost = ( $( '#heat_cost_sep' ).html() * 1 ) + ( $( '#daily_cost_sep' ).html() * 12 );
		$( '#holiday_cost_sep' ).html( holiday_cost.toFixed( 2 ) );
		holiday_cost = ( $( '#heat_cost_dec' ).html() * 1 ) + ( $( '#daily_cost_dec' ).html() * 40 );
		$( '#holiday_cost_xmas' ).html( holiday_cost.toFixed( 2 ) );

		holiday_cost = ( $( '#heat_cost_apr_cover' ).html() * 1 ) + ( $( '#daily_cost_apr_cover' ).html() * 12 );
		$( '#holiday_cost_apr_cover' ).html( holiday_cost.toFixed( 2 ) );
		holiday_cost = ( $( '#heat_cost_jun_cover' ).html() * 1 ) + ( $( '#daily_cost_jun_cover' ).html() * 12 );
		$( '#holiday_cost_jun_cover' ).html( holiday_cost.toFixed( 2 ) );
		holiday_cost = ( $( '#heat_cost_sep_cover' ).html() * 1 ) + ( $( '#daily_cost_sep_cover' ).html() * 12 );
		$( '#holiday_cost_sep_cover' ).html( holiday_cost.toFixed( 2 ) );
		holiday_cost = ( $( '#heat_cost_dec_cover' ).html() * 1 ) + ( $( '#daily_cost_dec_cover' ).html() * 40 );
		$( '#holiday_cost_xmas_cover' ).html( holiday_cost.toFixed( 2 ) );
		
		//
		// long weekend costs
		// the * 1 is to cast to a number
		
		$( '#weekend_cost_jan' ).html( ( $( '#heat_cost_jan' ).html() * 1 + $( '#daily_cost_jan' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_feb' ).html( ( $( '#heat_cost_feb' ).html() * 1 + $( '#daily_cost_feb' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_mar' ).html( ( $( '#heat_cost_mar' ).html() * 1 + $( '#daily_cost_mar' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_apr' ).html( ( $( '#heat_cost_apr' ).html() * 1 + $( '#daily_cost_apr' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_may' ).html( ( $( '#heat_cost_may' ).html() * 1 + $( '#daily_cost_may' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_jun' ).html( ( $( '#heat_cost_jun' ).html() * 1 + $( '#daily_cost_jun' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_jul' ).html( ( $( '#heat_cost_jul' ).html() * 1 + $( '#daily_cost_jul' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_aug' ).html( ( $( '#heat_cost_aug' ).html() * 1 + $( '#daily_cost_aug' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_sep' ).html( ( $( '#heat_cost_sep' ).html() * 1 + $( '#daily_cost_sep' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_oct' ).html( ( $( '#heat_cost_oct' ).html() * 1 + $( '#daily_cost_oct' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_nov' ).html( ( $( '#heat_cost_nov' ).html() * 1 + $( '#daily_cost_nov' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_dec' ).html( ( $( '#heat_cost_dec' ).html() * 1 + $( '#daily_cost_dec' ).html() * 1 ).toFixed( 2 ) );

		$( '#weekend_cost_jan_cover' ).html( ( $( '#heat_cost_jan_cover' ).html() * 1 + $( '#daily_cost_jan_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_feb_cover' ).html( ( $( '#heat_cost_feb_cover' ).html() * 1 + $( '#daily_cost_feb_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_mar_cover' ).html( ( $( '#heat_cost_mar_cover' ).html() * 1 + $( '#daily_cost_mar_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_apr_cover' ).html( ( $( '#heat_cost_apr_cover' ).html() * 1 + $( '#daily_cost_apr_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_may_cover' ).html( ( $( '#heat_cost_may_cover' ).html() * 1 + $( '#daily_cost_may_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_jun_cover' ).html( ( $( '#heat_cost_jun_cover' ).html() * 1 + $( '#daily_cost_jun_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_jul_cover' ).html( ( $( '#heat_cost_jul_cover' ).html() * 1 + $( '#daily_cost_jul_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_aug_cover' ).html( ( $( '#heat_cost_aug_cover' ).html() * 1 + $( '#daily_cost_aug_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_sep_cover' ).html( ( $( '#heat_cost_sep_cover' ).html() * 1 + $( '#daily_cost_sep_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_oct_cover' ).html( ( $( '#heat_cost_oct_cover' ).html() * 1 + $( '#daily_cost_oct_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_nov_cover' ).html( ( $( '#heat_cost_nov_cover' ).html() * 1 + $( '#daily_cost_nov_cover' ).html() * 1 ).toFixed( 2 ) );
		$( '#weekend_cost_dec_cover' ).html( ( $( '#heat_cost_dec_cover' ).html() * 1 + $( '#daily_cost_dec_cover' ).html() * 1 ).toFixed( 2 ) );
	},
	
	calculate_ave_temp_rise : function () {
	
		// check required fields
		if ( $( '.step2 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		function get_ave_temp_rise( heater_output ) {
		
			var pool_volume = $( '#average_length' ).val() * $( '#average_width' ).val()
				* $( '#average_depth' ).val();
			
			heater_output = heater_output * 1;
			if ( heater_output <= 0 ) {
				return 'N/A';
			}
			
			var ave_temp_rise = (
				1
				/
				(
					pool_volume
					* 1.16
					/ heater_output
					* 1.3
				)
			);
			return ave_temp_rise.toFixed( 2 );
		}
		
	
		$( '#ave_temp_rise_viron' ).html( get_ave_temp_rise( $( '#recommended_viron_output' ).val() ) );
		$( '#ave_temp_rise_conventional' ).html( get_ave_temp_rise( $( '#recommended_conventional_output' ).val() ) );
	},
	
	graph_temperature : function () {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var desired_temp = $( '#desired_temp' ).val();
		var location = this.locations[$( '#location' ).val()];
		
		var data = [
			{label: 'Desired Temperature', data:[[1,desired_temp],[12,desired_temp]]},
			{label: 'Unheated Pool Temperature', data:[
				[1,location[0]], 
				[2,location[1]], 
				[3,location[2]], 
				[4,location[3]], 
				[5,location[4]], 
				[6,location[5]], 
				[7,location[6]], 
				[8,location[7]], 
				[9,location[8]], 
				[10,location[9]], 
				[11,location[10]], 
				[12,location[11]]
			]}
		];
		var options = {
			lines: { show: true },
			xaxis: { 
				tickDecimals: 0, 
				tickSize: 1,
				ticks: [[1,'Jan'],[2,'Feb'],[3,'Mar'],[4,'Apr'],[5,'May'],[6,'Jun'],[7,'Jul'],[8,'Aug'],[9,'Sep'],[10,'Oct'],[11,'Nov'],[12,'Dec']]
			},
			yaxis: { 
				tickDecimals: 0, 
				tickSize: 5
			}
		};
			
		$.plot( $( '#graph_temperature' ), data, options );
	},

	graph_initial_mj_requirement : function() {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		var data = [
			{label: 'No Cover', data:[
				[1,$('#mj_required_jan').html()], 
				[2,$('#mj_required_feb').html()], 
				[3,$('#mj_required_mar').html()],
				[4,$('#mj_required_apr').html()],
				[5,$('#mj_required_may').html()],
				[6,$('#mj_required_jun').html()],
				[7,$('#mj_required_jul').html()],
				[8,$('#mj_required_aug').html()],
				[9,$('#mj_required_sep').html()],
				[10,$('#mj_required_oct').html()],
				[11,$('#mj_required_nov').html()],
				[12,$('#mj_required_dec').html()]
			]},
			{label: 'With Cover', data:[
				[1,$('#mj_required_jan_cover').html()], 
				[2,$('#mj_required_feb_cover').html()], 
				[3,$('#mj_required_mar_cover').html()],
				[4,$('#mj_required_apr_cover').html()],
				[5,$('#mj_required_may_cover').html()],
				[6,$('#mj_required_jun_cover').html()],
				[7,$('#mj_required_jul_cover').html()],
				[8,$('#mj_required_aug_cover').html()],
				[9,$('#mj_required_sep_cover').html()],
				[10,$('#mj_required_oct_cover').html()],
				[11,$('#mj_required_nov_cover').html()],
				[12,$('#mj_required_dec_cover').html()]
			]}
		];
		var options = {
			lines: { show: true },
			xaxis: { 
				tickDecimals: 0, 
				tickSize: 1,
				ticks: [[1,'Jan'],[2,'Feb'],[3,'Mar'],[4,'Apr'],[5,'May'],[6,'Jun'],[7,'Jul'],[8,'Aug'],[9,'Sep'],[10,'Oct'],[11,'Nov'],[12,'Dec']]
			},
			yaxis: { 
				tickDecimals: 0, 
				tickSize: 1000
			}
		};
			
		$.plot( $( '#graph_initial_mj_requirement' ), data, options );
	},
	
	graph_heat_up_cost : function () {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		var data = [
			{label: 'No Cover', data:[
				[1,$('#heat_cost_jan').html()], 
				[2,$('#heat_cost_feb').html()], 
				[3,$('#heat_cost_mar').html()],
				[4,$('#heat_cost_apr').html()],
				[5,$('#heat_cost_may').html()],
				[6,$('#heat_cost_jun').html()],
				[7,$('#heat_cost_jul').html()],
				[8,$('#heat_cost_aug').html()],
				[9,$('#heat_cost_sep').html()],
				[10,$('#heat_cost_oct').html()],
				[11,$('#heat_cost_nov').html()],
				[12,$('#heat_cost_dec').html()]
			]},
			{label: 'With Cover', data:[
				[1,$('#heat_cost_jan_cover').html()], 
				[2,$('#heat_cost_feb_cover').html()], 
				[3,$('#heat_cost_mar_cover').html()],
				[4,$('#heat_cost_apr_cover').html()],
				[5,$('#heat_cost_may_cover').html()],
				[6,$('#heat_cost_jun_cover').html()],
				[7,$('#heat_cost_jul_cover').html()],
				[8,$('#heat_cost_aug_cover').html()],
				[9,$('#heat_cost_sep_cover').html()],
				[10,$('#heat_cost_oct_cover').html()],
				[11,$('#heat_cost_nov_cover').html()],
				[12,$('#heat_cost_dec_cover').html()]
			]}
		];
		var options = {
			lines: { show: true },
			xaxis: { 
				tickDecimals: 0, 
				tickSize: 1,
				ticks: [[1,'Jan'],[2,'Feb'],[3,'Mar'],[4,'Apr'],[5,'May'],[6,'Jun'],[7,'Jul'],[8,'Aug'],[9,'Sep'],[10,'Oct'],[11,'Nov'],[12,'Dec']]
			},
			yaxis: { 
				tickDecimals: 2, 
				tickSize: 20,
				tickFormatter : function( val, axis ) {
					return '$' + val.toFixed( 2 );
				}
			}
		};
			
		$.plot( $( '#graph_heatup_cost' ), data, options );
	},

	graph_maintenance_cost : function () {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var desired_temp = $( '#desired_temp' ).val();
		var location = this.locations[$( '#location' ).val()];
		
		var data = [
			{label: 'No Cover', data:[
				[1.25,$('#daily_cost_jan').html() * 31], 
				[2.25,$('#daily_cost_feb').html() * 28], 
				[3.25,$('#daily_cost_mar').html() * 31],
				[4.25,$('#daily_cost_apr').html() * 30],
				[5.25,$('#daily_cost_may').html() * 31],
				[6.25,$('#daily_cost_jun').html() * 30],
				[7.25,$('#daily_cost_jul').html() * 31],
				[8.25,$('#daily_cost_aug').html() * 31],
				[9.25,$('#daily_cost_sep').html() * 30],
				[10.25,$('#daily_cost_oct').html() * 31],
				[11.25,$('#daily_cost_nov').html() * 30],
				[12.25,$('#daily_cost_dec').html() * 31]
			]},
			{label: 'With Cover', data:[
				[1.5,$('#daily_cost_jan_cover').html() * 31], 
				[2.5,$('#daily_cost_feb_cover').html() * 28], 
				[3.5,$('#daily_cost_mar_cover').html() * 31],
				[4.5,$('#daily_cost_apr_cover').html() * 30],
				[5.5,$('#daily_cost_may_cover').html() * 31],
				[6.5,$('#daily_cost_jun_cover').html() * 30],
				[7.5,$('#daily_cost_jul_cover').html() * 31],
				[8.5,$('#daily_cost_aug_cover').html() * 31],
				[9.5,$('#daily_cost_sep_cover').html() * 30],
				[10.5,$('#daily_cost_oct_cover').html() * 31],
				[11.5,$('#daily_cost_nov_cover').html() * 30],
				[12.5,$('#daily_cost_dec_cover').html() * 31]
			]}
		];
		var options = {
			lines: { show: false },
			bars: { show: true, barWidth: 0.25 },
			xaxis: { 
				tickDecimals: 0, 
				tickSize: 1,
				ticks: [[1.5,'Jan'],[2.5,'Feb'],[3.5,'Mar'],[4.5,'Apr'],[5.5,'May'],[6.5,'Jun'],[7.5,'Jul'],[8.5,'Aug'],[9.5,'Sep'],[10.5,'Oct'],[11.5,'Nov'],[12.5,'Dec']]
			},
			yaxis: { 
				tickDecimals: 0, 
				tickSize: 200,
				tickFormatter : function( val, axis ) {
					return '$' + val.toFixed( 2 );
				}
			}
		};
			
		$.plot( $( '#graph_maintenance_cost' ), data, options );
	}
};

$( document ).ready( pool_heater_sizing_calculator.init );
