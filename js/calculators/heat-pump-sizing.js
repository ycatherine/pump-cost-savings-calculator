/**
*	Heat Pump Sizing calculator.
*	Unfortunately the Excel document this was ported from has some magic numbers (i.e. their 
*	purpose or meaning was unknown), so they have been copied as is.
*	@author Chris Botman <chris@botman.com.au> on behalf of Wordplay Media
*/

var heat_pump_sizing_calculator = {

	init : function() {
	
		// init validation and triggers
		$( '#heat-pump-sizing' ).validate();
		$( 'input, select' ).change( function() {
			heat_pump_sizing_calculator.update();
		});
		
		// all fields are preset, so perform first calculation
		heat_pump_sizing_calculator.update();
	},
	
	update : function () {
	
		this.calculate_kw_required();
		this.calculate_recommended_model();
		this.calculate_costs();
		this.calculate_ave_temps();
		this.graph_initial_kw_requirement();
		this.graph_heat_up_cost();
		this.graph_temperature();
		this.graph_maintenance_cost();
	},
	
	locations : {
		// These are monthly average temperatures, jan to dec
		"Adelaide" : [23.1, 22.6, 20.4, 17.6, 13.7, 11.4, 10.4, 11.3, 13.8, 16.8, 19.1, 20.8],
		"Brisbane" : [25.6, 25.3, 23.8, 21.5, 17.6, 14.8, 13.5, 15.4, 18.8, 21.9, 23.9, 25.1],
		"Melbourne" : [20.1, 19.8, 17.6, 14.6, 11.2, 9, 8.2, 9.5, 11.9, 14.6, 16.5, 18.5],
		"Perth" : [23.9, 23.4, 21.2, 17.9, 15.2, 13.3, 11.9, 12.6, 14.5, 17.3, 20, 22.7],
		"Sydney" : [22.9, 23, 21.4, 18.4, 14.3, 12, 10.9, 12.5, 15.4, 18.6, 20.7, 22.1],
		"FNQ Coastal" : [25.6, 25.3, 23.8, 23.5, 20.6, 17.8, 16.5, 18.4, 21.8, 24.9, 25.9, 27.1]
	},
	
	calculate_kw_required : function() {

		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var total_meters = $( '#average_length' ).val() * $( '#average_width' ).val() * 
			$( '#average_depth' ).val();
		var desired_temp = $( '#desired_temp' ).val();
		var average_temp = $( '#location_ave_temp' ).val();
		var location = this.locations[$( '#location' ).val()];
		
		function get_kw_required( average_temp, is_covered ) {
			var kw_required = (
				total_meters
				* 1.16 // Magic number...
				* 1.3 // Magic number...
				* ( desired_temp - average_temp )
			);
			
			if ( is_covered ) {
				kw_required *= 0.885;
			}
			
			return Math.round( kw_required );
		}
		
		$( '#kw_required_jan' ).html( get_kw_required( location[0], false ) );
		$( '#kw_required_feb' ).html( get_kw_required( location[1], false ) );
		$( '#kw_required_mar' ).html( get_kw_required( location[2], false ) );
		$( '#kw_required_apr' ).html( get_kw_required( location[3], false ) );
		$( '#kw_required_may' ).html( get_kw_required( location[4], false ) );
		$( '#kw_required_jun' ).html( get_kw_required( location[5], false ) );
		$( '#kw_required_jul' ).html( get_kw_required( location[6], false ) );
		$( '#kw_required_aug' ).html( get_kw_required( location[7], false ) );
		$( '#kw_required_sep' ).html( get_kw_required( location[8], false ) );
		$( '#kw_required_oct' ).html( get_kw_required( location[9], false ) );
		$( '#kw_required_nov' ).html( get_kw_required( location[10], false ) );
		$( '#kw_required_dec' ).html( get_kw_required( location[11], false ) );
		
		$( '#kw_required_jan_cover' ).html( get_kw_required( location[0], true ) );
		$( '#kw_required_feb_cover' ).html( get_kw_required( location[1], true ) );
		$( '#kw_required_mar_cover' ).html( get_kw_required( location[2], true ) );
		$( '#kw_required_apr_cover' ).html( get_kw_required( location[3], true ) );
		$( '#kw_required_may_cover' ).html( get_kw_required( location[4], true ) );
		$( '#kw_required_jun_cover' ).html( get_kw_required( location[5], true ) );
		$( '#kw_required_jul_cover' ).html( get_kw_required( location[6], true ) );
		$( '#kw_required_aug_cover' ).html( get_kw_required( location[7], true ) );
		$( '#kw_required_sep_cover' ).html( get_kw_required( location[8], true ) );
		$( '#kw_required_oct_cover' ).html( get_kw_required( location[9], true ) );
		$( '#kw_required_nov_cover' ).html( get_kw_required( location[10], true ) );
		$( '#kw_required_dec_cover' ).html( get_kw_required( location[11], true ) );
	},
	
	calculate_recommended_model : function() {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		var recommended_model = 'Contact your local AstralPool office';
		
		var kw_required = 0;
		var swimming_season = $( '#swimming_season' ).val();
		
		if ( swimming_season == '1' ) { // solar replacement

			kw_required = $( '#kw_required_oct' ).html() / 48;
		}
		else if ( swimming_season == '2' ) { // extended
		
			kw_required = $( '#kw_required_sep' ).html() / 48;
		}
		else if ( swimming_season == '3' ) { // all year around
		
			kw_required = $( '#kw_required_jul' ).html() / 48;
		}
		
		if ( $( '#power_phase' ).val() == 'Single Phase' ) {
			
			if ( this.output_condition_1['BPM 400'] > kw_required ) {
				recommended_model = 'BPM 400';
			}
			else if ( this.output_condition_1['BPM 600'] > kw_required ) {
				recommended_model = 'BPM 600';
			}
			else if ( this.output_condition_1['BPM 700'] > kw_required ) {
				recommended_model = 'BPM 700';
			}
			else if ( this.output_condition_1['BPM 800'] > kw_required ) {
				recommended_model = 'BPM 800';
			}
		}
		else { // Three Phase
		
			if ( this.output_condition_1['BPT 700'] > kw_required ) {
				recommended_model = 'BPT 700';
			}
			else if ( this.output_condition_1['BPT 800'] > kw_required ) {
				recommended_model = 'BPT 800';
			}
			else if ( this.output_condition_1['BPT 900'] > kw_required ) {
				recommended_model = 'BPT 900';
			}
		}
		
		$( '#recommended_model' ).html( recommended_model );
	},
	
	calculate_costs : function() {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		function get_heat_up_cost( kw_required, is_may_to_sep ) {
		
			var power_cost = $( '#power_cost' ).val();
			var magic_number = is_may_to_sep ? 4 : 5;
			var heat_up_cost = (
				kw_required 
				* power_cost
				/ 100
				/ magic_number
			);
			return heat_up_cost.toFixed( 2 );
		}
		
		$( '#heat_up_cost_jan' ).html( get_heat_up_cost( $( '#kw_required_jan' ).html(), false ) );
		$( '#heat_up_cost_feb' ).html( get_heat_up_cost( $( '#kw_required_feb' ).html(), false ) );
		$( '#heat_up_cost_mar' ).html( get_heat_up_cost( $( '#kw_required_mar' ).html(), false ) );
		$( '#heat_up_cost_apr' ).html( get_heat_up_cost( $( '#kw_required_apr' ).html(), false ) );
		$( '#heat_up_cost_may' ).html( get_heat_up_cost( $( '#kw_required_may' ).html(), true ) );
		$( '#heat_up_cost_jun' ).html( get_heat_up_cost( $( '#kw_required_jun' ).html(), true ) );
		$( '#heat_up_cost_jul' ).html( get_heat_up_cost( $( '#kw_required_jul' ).html(), true ) );
		$( '#heat_up_cost_aug' ).html( get_heat_up_cost( $( '#kw_required_aug' ).html(), true ) );
		$( '#heat_up_cost_sep' ).html( get_heat_up_cost( $( '#kw_required_sep' ).html(), true ) );
		$( '#heat_up_cost_oct' ).html( get_heat_up_cost( $( '#kw_required_oct' ).html(), false ) );
		$( '#heat_up_cost_nov' ).html( get_heat_up_cost( $( '#kw_required_nov' ).html(), false ) );
		$( '#heat_up_cost_dec' ).html( get_heat_up_cost( $( '#kw_required_dec' ).html(), false ) );
		
		$( '#heat_up_cost_jan_cover' ).html( get_heat_up_cost( $( '#kw_required_jan_cover' ).html(), false ) );
		$( '#heat_up_cost_feb_cover' ).html( get_heat_up_cost( $( '#kw_required_feb_cover' ).html(), false ) );
		$( '#heat_up_cost_mar_cover' ).html( get_heat_up_cost( $( '#kw_required_mar_cover' ).html(), false ) );
		$( '#heat_up_cost_apr_cover' ).html( get_heat_up_cost( $( '#kw_required_apr_cover' ).html(), false ) );
		$( '#heat_up_cost_may_cover' ).html( get_heat_up_cost( $( '#kw_required_may_cover' ).html(), true ) );
		$( '#heat_up_cost_jun_cover' ).html( get_heat_up_cost( $( '#kw_required_jun_cover' ).html(), true ) );
		$( '#heat_up_cost_jul_cover' ).html( get_heat_up_cost( $( '#kw_required_jul_cover' ).html(), true ) );
		$( '#heat_up_cost_aug_cover' ).html( get_heat_up_cost( $( '#kw_required_aug_cover' ).html(), true ) );
		$( '#heat_up_cost_sep_cover' ).html( get_heat_up_cost( $( '#kw_required_sep_cover' ).html(), true ) );
		$( '#heat_up_cost_oct_cover' ).html( get_heat_up_cost( $( '#kw_required_oct_cover' ).html(), false ) );
		$( '#heat_up_cost_nov_cover' ).html( get_heat_up_cost( $( '#kw_required_nov_cover' ).html(), false ) );
		$( '#heat_up_cost_dec_cover' ).html( get_heat_up_cost( $( '#kw_required_dec_cover' ).html(), false ) );

		var power_cost = $( '#power_cost' ).val();
		var average_length = $( '#average_length' ).val();
		var average_width = $( '#average_width' ).val();
		// has depth been left out on the Excel sheet on purpose?

		function get_daily_cost( kw_required, is_covered, is_may_to_sep ) {
		
			var power_cost = $( '#power_cost' ).val();
			var magic_number = ( is_may_to_sep == true ) ? 4 : 4.8;
			var daily_cost = (
				kw_required
				* 0.3
				/ magic_number
				* power_cost
				/ 100
			);
			
			if ( is_covered ) {
				daily_cost *= 0.6;
			}
			
			return daily_cost.toFixed( 2 );
		}
		
		$( '#daily_cost_jan' ).html( get_daily_cost( $( '#kw_required_jan' ).html(), false ) );
		$( '#daily_cost_feb' ).html( get_daily_cost( $( '#kw_required_feb' ).html(), false ) );
		$( '#daily_cost_mar' ).html( get_daily_cost( $( '#kw_required_mar' ).html(), false ) );
		$( '#daily_cost_apr' ).html( get_daily_cost( $( '#kw_required_apr' ).html(), false ) );
		$( '#daily_cost_may' ).html( get_daily_cost( $( '#kw_required_may' ).html(), false, true ) );
		$( '#daily_cost_jun' ).html( get_daily_cost( $( '#kw_required_jun' ).html(), false, true ) );
		$( '#daily_cost_jul' ).html( get_daily_cost( $( '#kw_required_jul' ).html(), false, true ) );
		$( '#daily_cost_aug' ).html( get_daily_cost( $( '#kw_required_aug' ).html(), false, true ) );
		$( '#daily_cost_sep' ).html( get_daily_cost( $( '#kw_required_sep' ).html(), false, true ) );
		$( '#daily_cost_oct' ).html( get_daily_cost( $( '#kw_required_oct' ).html(), false ) );
		$( '#daily_cost_nov' ).html( get_daily_cost( $( '#kw_required_nov' ).html(), false ) );
		$( '#daily_cost_dec' ).html( get_daily_cost( $( '#kw_required_dec' ).html(), false ) );
		
		$( '#daily_cost_jan_cover' ).html( get_daily_cost( $( '#kw_required_jan' ).html(), true ) );
		$( '#daily_cost_feb_cover' ).html( get_daily_cost( $( '#kw_required_feb' ).html(), true ) );
		$( '#daily_cost_mar_cover' ).html( get_daily_cost( $( '#kw_required_mar' ).html(), true ) );
		$( '#daily_cost_apr_cover' ).html( get_daily_cost( $( '#kw_required_apr' ).html(), true ) );
		$( '#daily_cost_may_cover' ).html( get_daily_cost( $( '#kw_required_may' ).html(), true, true ) );
		$( '#daily_cost_jun_cover' ).html( get_daily_cost( $( '#kw_required_jun' ).html(), true, true ) );
		$( '#daily_cost_jul_cover' ).html( get_daily_cost( $( '#kw_required_jul' ).html(), true, true ) );
		$( '#daily_cost_aug_cover' ).html( get_daily_cost( $( '#kw_required_aug' ).html(), true, true ) );
		$( '#daily_cost_sep_cover' ).html( get_daily_cost( $( '#kw_required_sep' ).html(), true, true ) );
		$( '#daily_cost_oct_cover' ).html( get_daily_cost( $( '#kw_required_oct' ).html(), true ) );
		$( '#daily_cost_nov_cover' ).html( get_daily_cost( $( '#kw_required_nov' ).html(), true ) );
		$( '#daily_cost_dec_cover' ).html( get_daily_cost( $( '#kw_required_dec' ).html(), true ) );
	},

	output_condition_1 : { 
	//  model 	kW output at 27 degree air temperature, 63% RH & 27 degree entering water temperature
		"BPM 400" : 9,
		"BPM 600" : 14,
		"BPM 700" : 19,
		"BPM 800" : 25,
		"BPT 700" : 19,
		"BPT 800" : 25,
		"BPT 900" : 30
	},
	
	output_condition_2 : { 
	//  model 	kW output at 10 degree air temperature, 63% RH & 27 degree entering water temperature
		"BPM 400" : 9,
		"BPM 600" : 10,
		"BPM 700" : 14,
		"BPM 800" : 18,
		"BPT 700" : 14,
		"BPT 800" : 18,
		"BPT 900" : 22
	},
	
	calculate_ave_temps : function () {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var recommended_model = $( '#recommended_model' ).html();
		var total_meters = $( '#average_length' ).val() * $( '#average_width' ).val() * 
			$( '#average_depth' ).val();

		// ensure there is a recommended pump available for each scenario
		
		var pump_kw_output_condition_1 = this.output_condition_1["" + recommended_model];
		if ( pump_kw_output_condition_1 == undefined ) {
			$( '#ave_temp_1' ).html( 'N/A' );
		}
		else {
			var ave_temp_1 = (
				1
				/ 
				( 
					total_meters 
					* 1.16 // Magic numbers
					/ pump_kw_output_condition_1
					* 1.3 
				)
			);
			$( '#ave_temp_1' ).html( ave_temp_1.toFixed( 2 ) );
		}

		var pump_kw_output_condition_2 = this.output_condition_2["" + recommended_model];
		if ( pump_kw_output_condition_2 == undefined ) {
			$( '#ave_temp_2' ).html( 'N/A' );
		}
		else {
			var ave_temp_2 = (
				1
				/ 
				( 
					total_meters 
					* 1.16 // Magic numbers
					/ pump_kw_output_condition_2
					* 1.3 
				)
			);
			$( '#ave_temp_2' ).html( ave_temp_2.toFixed( 2 ) );
		}

	},
	
	graph_initial_kw_requirement : function() {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		var data = [
			{label: 'No Cover', data:[
				[1,$('#kw_required_jan').html()], 
				[2,$('#kw_required_feb').html()], 
				[3,$('#kw_required_mar').html()],
				[4,$('#kw_required_apr').html()],
				[5,$('#kw_required_may').html()],
				[6,$('#kw_required_jun').html()],
				[7,$('#kw_required_jul').html()],
				[8,$('#kw_required_aug').html()],
				[9,$('#kw_required_sep').html()],
				[10,$('#kw_required_oct').html()],
				[11,$('#kw_required_nov').html()],
				[12,$('#kw_required_dec').html()]
			]},
			{label: 'With Cover', data:[
				[1,$('#kw_required_jan_cover').html()], 
				[2,$('#kw_required_feb_cover').html()], 
				[3,$('#kw_required_mar_cover').html()],
				[4,$('#kw_required_apr_cover').html()],
				[5,$('#kw_required_may_cover').html()],
				[6,$('#kw_required_jun_cover').html()],
				[7,$('#kw_required_jul_cover').html()],
				[8,$('#kw_required_aug_cover').html()],
				[9,$('#kw_required_sep_cover').html()],
				[10,$('#kw_required_oct_cover').html()],
				[11,$('#kw_required_nov_cover').html()],
				[12,$('#kw_required_dec_cover').html()]
			]}
		];
		var options = {
			lines: { show: true },
			xaxis: { 
				tickDecimals: 0, 
				tickSize: 1,
				ticks: [[1,'Jan'],[2,'Feb'],[3,'Mar'],[4,'Apr'],[5,'May'],[6,'Jun'],[7,'Jul'],[8,'Aug'],[9,'Sep'],[10,'Oct'],[11,'Nov'],[12,'Dec']]
			},
			yaxis: { 
				tickDecimals: 0, 
				tickSize: 100
			}
		};
			
		$.plot( $( '#graph_initial_kw_requirement' ), data, options );
	},
	
	graph_heat_up_cost : function () {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		var data = [
			{label: 'No Cover', data:[
				[1,$('#heat_up_cost_jan').html()], 
				[2,$('#heat_up_cost_feb').html()], 
				[3,$('#heat_up_cost_mar').html()],
				[4,$('#heat_up_cost_apr').html()],
				[5,$('#heat_up_cost_may').html()],
				[6,$('#heat_up_cost_jun').html()],
				[7,$('#heat_up_cost_jul').html()],
				[8,$('#heat_up_cost_aug').html()],
				[9,$('#heat_up_cost_sep').html()],
				[10,$('#heat_up_cost_oct').html()],
				[11,$('#heat_up_cost_nov').html()],
				[12,$('#heat_up_cost_dec').html()]
			]},
			{label: 'With Cover', data:[
				[1,$('#heat_up_cost_jan_cover').html()], 
				[2,$('#heat_up_cost_feb_cover').html()], 
				[3,$('#heat_up_cost_mar_cover').html()],
				[4,$('#heat_up_cost_apr_cover').html()],
				[5,$('#heat_up_cost_may_cover').html()],
				[6,$('#heat_up_cost_jun_cover').html()],
				[7,$('#heat_up_cost_jul_cover').html()],
				[8,$('#heat_up_cost_aug_cover').html()],
				[9,$('#heat_up_cost_sep_cover').html()],
				[10,$('#heat_up_cost_oct_cover').html()],
				[11,$('#heat_up_cost_nov_cover').html()],
				[12,$('#heat_up_cost_dec_cover').html()]
			]}
		];
		var options = {
			lines: { show: true },
			xaxis: { 
				tickDecimals: 0, 
				tickSize: 1,
				ticks: [[1,'Jan'],[2,'Feb'],[3,'Mar'],[4,'Apr'],[5,'May'],[6,'Jun'],[7,'Jul'],[8,'Aug'],[9,'Sep'],[10,'Oct'],[11,'Nov'],[12,'Dec']]
			},
			yaxis: { 
				tickDecimals: 2, 
				tickSize: 5,
				tickFormatter : function( val, axis ) {
					return '$' + val.toFixed( 2 );
				}
			}
		};
			
		$.plot( $( '#graph_heatup_cost' ), data, options );
	},
	
	graph_temperature : function () {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var desired_temp = $( '#desired_temp' ).val();
		var location = this.locations[$( '#location' ).val()];

		/*
		* 	The result is based roughly on the location average for each month + 10 degrees.
		*	But then there are a bunch of hard-coded exceptions. 
		*/
		function get_acheivable( location, month ) {

			var achievable = location[month] + 10;

			var swimming_season = $( '#swimming_season' ).val();
			var location_name = $( '#location' ).val();
			
			if ( swimming_season == '3' ) { // all year around

				var desired_temp = $( '#desired_temp' ).val();
				
				if ( location_name == 'Adelaide' ) {
					if ( month >= 4 && month <= 9 ) {
						achievable = desired_temp;
					}
				}
				else if ( location_name == 'Brisbane' ) {
					if ( month >= 4 && month <= 7 ) {
						achievable = desired_temp;
					}
				}
				else if ( location_name == 'Melbourne' ) {
					if ( month >= 2 && month <= 10 ) {
						achievable = desired_temp;
					}
				}
				else if ( location_name == 'Perth' ) {
					if ( month >= 4 && month <= 9 ) {
						achievable = desired_temp;
					}
				}
				else if ( location_name == 'Sydney' ) {
					if ( month >= 4 && month <= 8 ) {
						achievable = desired_temp;
					}
				}
				else if ( location_name == 'FNQ Coastal' ) {
					if ( month >= 5 && month <= 6 ) {
						achievable = desired_temp;
					}
				}
			}
			else { // solar or extended

				if ( location_name == 'Brisbane' ) {
					if ( month <= 2 || month >= 10 ) {
						achievable = 33;
					}
				}
				else if ( location_name == 'FNQ Coastal' ) {
					if ( month <= 3 || month >= 9 ) {
						achievable = 33;
					}
				}
			}
			
			return achievable;
		}
		
		var data = [
			{label: 'Desired Temperature', data:[[1,desired_temp],[12,desired_temp]]},
			{label: 'Unheated Pool Temperature', data:[
				[1,location[0]], 
				[2,location[1]], 
				[3,location[2]], 
				[4,location[3]], 
				[5,location[4]], 
				[6,location[5]], 
				[7,location[6]], 
				[8,location[7]], 
				[9,location[8]], 
				[10,location[9]], 
				[11,location[10]], 
				[12,location[11]]
			]},
			{label: 'Acheivable Water<br> Temperature with Heat Pump', data:[
				[1,get_acheivable( location, 0 )], 
				[2,get_acheivable( location, 1 )], 
				[3,get_acheivable( location, 2 )], 
				[4,get_acheivable( location, 3 )], 
				[5,get_acheivable( location, 4 )], 
				[6,get_acheivable( location, 5 )], 
				[7,get_acheivable( location, 6 )], 
				[8,get_acheivable( location, 7 )], 
				[9,get_acheivable( location, 8 )], 
				[10,get_acheivable( location, 9 )], 
				[11,get_acheivable( location, 10 )], 
				[12,get_acheivable( location, 11 )]
			]}
		];
		var options = {
			lines: { show: true },
			xaxis: { 
				tickDecimals: 0, 
				tickSize: 1,
				ticks: [[1,'Jan'],[2,'Feb'],[3,'Mar'],[4,'Apr'],[5,'May'],[6,'Jun'],[7,'Jul'],[8,'Aug'],[9,'Sep'],[10,'Oct'],[11,'Nov'],[12,'Dec']]
			},
			yaxis: { 
				tickDecimals: 0, 
				tickSize: 5
			}
		};
			
		$.plot( $( '#graph_temperature' ), data, options );
	},
	
	graph_maintenance_cost : function () {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var desired_temp = $( '#desired_temp' ).val();
		var location = this.locations[$( '#location' ).val()];
		
		var data = [
			{label: 'No Cover', data:[
				[1.25,$('#daily_cost_jan').html() * 31], 
				[2.25,$('#daily_cost_feb').html() * 28], 
				[3.25,$('#daily_cost_mar').html() * 31],
				[4.25,$('#daily_cost_apr').html() * 30],
				[5.25,$('#daily_cost_may').html() * 31],
				[6.25,$('#daily_cost_jun').html() * 30],
				[7.25,$('#daily_cost_jul').html() * 31],
				[8.25,$('#daily_cost_aug').html() * 31],
				[9.25,$('#daily_cost_sep').html() * 30],
				[10.25,$('#daily_cost_oct').html() * 31],
				[11.25,$('#daily_cost_nov').html() * 30],
				[12.25,$('#daily_cost_dec').html() * 31]
			]},
			{label: 'With Cover', data:[
				[1.5,$('#daily_cost_jan_cover').html() * 31], 
				[2.5,$('#daily_cost_feb_cover').html() * 28], 
				[3.5,$('#daily_cost_mar_cover').html() * 31],
				[4.5,$('#daily_cost_apr_cover').html() * 30],
				[5.5,$('#daily_cost_may_cover').html() * 31],
				[6.5,$('#daily_cost_jun_cover').html() * 30],
				[7.5,$('#daily_cost_jul_cover').html() * 31],
				[8.5,$('#daily_cost_aug_cover').html() * 31],
				[9.5,$('#daily_cost_sep_cover').html() * 30],
				[10.5,$('#daily_cost_oct_cover').html() * 31],
				[11.5,$('#daily_cost_nov_cover').html() * 30],
				[12.5,$('#daily_cost_dec_cover').html() * 31]
			]}
		];
		var options = {
			lines: { show: false },
			bars: { show: true, barWidth: 0.25 },
			xaxis: { 
				tickDecimals: 0, 
				tickSize: 1,
				ticks: [[1.5,'Jan'],[2.5,'Feb'],[3.5,'Mar'],[4.5,'Apr'],[5.5,'May'],[6.5,'Jun'],[7.5,'Jul'],[8.5,'Aug'],[9.5,'Sep'],[10.5,'Oct'],[11.5,'Nov'],[12.5,'Dec']]
			},
			yaxis: { 
				tickDecimals: 0, 
				tickSize: 50,
				tickFormatter : function( val, axis ) {
					return '$' + val.toFixed( 2 );
				}
			}
		};
			
		$.plot( $( '#graph_maintenance_cost' ), data, options );
	}
};

$( document ).ready( heat_pump_sizing_calculator.init );
