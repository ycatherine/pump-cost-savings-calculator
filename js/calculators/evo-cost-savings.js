/**
*	Cost Savings calculator.
*	@author Chris Botman <chris@botman.com.au> and Mark Wheeler <http://www.markwheeler.com.au/>
*/

var cost_saving_calculator = {

	init : function() {

		// init validation and triggers
		$( '#cost-savings' ).validate();
		$( 'input, select' ).change( function() {
			cost_saving_calculator.update();
		});

		// all fields are preset, so perform first calculation
		cost_saving_calculator.update();
	},

	update : function () {

		this.update_pump_speed_display();
		this.calculate_total_litres();
		this.calculate_water_turnovers();
		this.calculate_annual_power_cost();
		this.calculate_cost_savings();
		this.calculate_co2_emissions();
		this.calculate_emission_savings();
		this.graph_running_cost();
	},

	performance : {
	//  hp 		litres
		0.75 	: 740,
		1 		: 925,
		1.5 	: 1110,
		2 		: 1480
	},

	evo_power_use : {
	//	model	max watts
		280		: 1078,
		300		: 1078,
		600		: 1886
	},

	evo_max_pump_flow : {
	// 	model	max litres/min
		280		: 1121,
		300		: 1210,
		600		: 1850
	},

	update_pump_speed_display : function() {

		if ( $( '#factory-settings' ).is( ':checked' ) ) {

			$( '#factory_speed_wrapper' ).removeClass( 'invisible' );
			$( '#custom_speed_wrapper' ).addClass( 'invisible' );

			// Set the hidden slider values to match the factory setting
			$( '#hidden-max' ).val( 10545 );
			$( '#hidden-min' ).val( $( '#preset_speed' ).val() );
		}
		else {

			$( '#factory_speed_wrapper' ).addClass( 'invisible' );
			$( '#custom_speed_wrapper' ).removeClass( 'invisible' );

			// Set the hidden slider values to match the slider itself
			$( '#hidden-min' ).val( $( ".slider-min" ).slider( "value" ) );
			$( '#hidden-max' ).val( $( ".slider-max" ).slider( "value" ) );
		}
	},

	calculate_total_litres : function() {

		// check required fields
		if ( $( '.step1 input' ).hasClass( 'error' ) ) {
			return;
		}

		// calc and massage total litres
		var total_litres = $( '#average_length' ).val() * $( '#average_width' ).val() *
			$( '#average_depth' ).val() * 1000;

		if ( total_litres < 0 ) {
			total_litres = 0;
		}
		else {
			total_litres = Math.round( total_litres );
		}

		// set result
		$( '#total_litres' ).html( total_litres * 3.7 );

	},

	calculate_water_turnovers : function() {

		// check required fields
		if ( $( '.step1 input, .step4 input' ).hasClass( 'error' ) ) {
			return;
		}

		//
		// old pump turnovers
		var litres_per_minute = this.performance[$( '#pump_size' ).val()];
		var operating_hours = $( '#hours_summer' ).val();
		var total_litres = $( '#total_litres' ).html();

		var water_turnovers = (
			litres_per_minute
			* 60 // minutes
			* operating_hours
			/ total_litres
		);
		$( '#water_turnovers' ).html( water_turnovers.toFixed( 2 ) );

		//
		// evo turnovers

		// high turnover
		var evo_high_litres_per_minute = this.evo_max_pump_flow[$( '#pump_model' ).val()];
		evo_high_litres_per_minute *= ( $( '#hidden-max' ).val() / 10545 ); // 2850 = system max
		var evo_high_operating_hours = $( '#high_hours' ).val();

		var high_evo_water_turnovers = (
			evo_high_litres_per_minute
			* 60
			* evo_high_operating_hours
			/ total_litres
		);

		// low turnover
		var evo_low_litres_per_minute = this.evo_max_pump_flow[$( '#pump_model' ).val()];
		evo_low_litres_per_minute *= ( $( '#hidden-min' ).val() / 10545 ); // 2850 = system max
		var evo_low_operating_hours = $( '#low_hours' ).val();

		var low_evo_water_turnovers = (
			evo_low_litres_per_minute
			* 60
			* evo_low_operating_hours
			/ total_litres
		);

		var total_turnovers = high_evo_water_turnovers + low_evo_water_turnovers;

		$( '#evo_water_turnovers' ).html( total_turnovers.toFixed( 2 ) );
	},

	calculate_annual_power_cost : function() {

		// check required fields
		if ( $( '.step1 input, .step4 input' ).hasClass( 'error' ) ) {
			return;
		}

		//
		// Current power cost
		var kwhr = $( '#pump_size' ).val();
		var hours_summer = $( '#hours_summer' ).val();
		var hours_winter = $( '#hours_winter' ).val();
		var cents_per_kwhr = $( '#cents_per_kwhr' ).val();
		var total_hours = 	(
			( hours_summer * 30 * 7 )
			+ ( hours_winter * 30 * 5 )
		);

		var annual_power_cost = (
			total_hours
			* kwhr
			* cents_per_kwhr
			/ 100 // cents to dollars
		);
		$( '#annual_power_cost' ).html( annual_power_cost.toFixed( 2 ) );

		//
		// eVo power cost

		// high
		var high_evo_kwhr = this.evo_power_use[$( '#pump_model' ).val()];
		var high_speed = parseFloat( $( '#hidden-max' ).val() );
		high_evo_kwhr = high_evo_kwhr * Math.pow( ( high_speed / 10545 ), 3 ) / 1000; // 2850 = system max;
		var high_total_hours = 	(
			( $( '#high_hours' ).val() * 30 * 7 )
			+ ( $( '#high_hours' ).val() * 30 * 5 )
		);

		var high_evo_annual_power_cost = (
			high_total_hours
			* high_evo_kwhr
			* cents_per_kwhr
			/ 100 // cents to dollars
		);

		// low
		var low_evo_kwhr = this.evo_power_use[$( '#pump_model' ).val()];
		var low_speed = parseFloat( $( '#hidden-min' ).val() );
		low_evo_kwhr = low_evo_kwhr * Math.pow( ( low_speed / 10545 ), 3 ) / 1000; // 2850 = system max;
		var low_total_hours = 	(
			( $( '#low_hours' ).val() * 30 * 7 )
			+ ( $( '#low_hours' ).val() * 30 * 5 )
		);

		var low_evo_annual_power_cost = (
			low_total_hours
			* low_evo_kwhr
			* cents_per_kwhr
			/ 100 // cents to dollars
		);

		// total
		var evo_annual_power_cost = high_evo_annual_power_cost + low_evo_annual_power_cost;
		$( '#evo_annual_power_cost' ).html( evo_annual_power_cost.toFixed( 2 ) );

		//
		// Saving
		var saving_percent = ( annual_power_cost - evo_annual_power_cost ) / annual_power_cost * 100;
		if ( saving_percent < 0 ) {
			saving_percent = 0;
		}
		$( '#saving_percent' ).html( saving_percent.toFixed( 0 ) );
	},

	calculate_cost_savings : function() {

		// check required fields
		if ( $( '.step1 input, .step4 input' ).hasClass( 'error' ) ) {
			return;
		}

		var annual_saving = $( '#annual_power_cost' ).html() - $( '#evo_annual_power_cost' ).html();
		$( '#annual_saving' ).html( annual_saving.toFixed( 2 ) );

		var monthly_saving = annual_saving / 12;
		$( '#monthly_saving' ).html( monthly_saving.toFixed( 2 ) );
	},

	calculate_co2_emissions : function() {

		// check required fields
		if ( $( '.step1 input, .step4 input' ).hasClass( 'error' ) ) {
			return;
		}

		//
		// current pump
		var kwhr = $( '#pump_size' ).val();
		//var total_hours = ( 1 * $( '#hours_summer' ).val() ) + ( 1 * $( '#hours_winter' ).val() );
		var total_hours = ( 1 * $( '#hours_summer' ).val() );

		var co2_emissions = (
			kwhr
			* total_hours
			* 1.1 // CO2 at 1.1kg/kw hr
			* 365 // days
		);
		$( '#annual_co2_emissions' ).html( Math.round( co2_emissions ) );

		//
		// evo

		// high
		var high_evo_kwhr = this.evo_power_use[$( '#pump_model' ).val()];
		var high_speed = parseFloat( $( '#hidden-max' ).val() );
		high_evo_kwhr = high_evo_kwhr * Math.pow( ( high_speed / 10545 ), 3 ) / 1000; // 2850 = system max;

		var high_evo_co2_emissions = (
			$( '#high_hours' ).val()
			* high_evo_kwhr
			* 1.1 // CO2 at 1.1kg/kw hr
			* 365 // cents to dollars
		);

		// low
		var low_evo_kwhr = this.evo_power_use[$( '#pump_model' ).val()];
		var low_speed = parseFloat( $( '#hidden-min' ).val() );
		low_evo_kwhr = low_evo_kwhr * Math.pow( ( low_speed / 10545 ), 3 ) / 1000; // 2850 = system max;

		var low_evo_co2_emissions = (
			$( '#low_hours' ).val()
			* low_evo_kwhr
			* 1.1 // CO2 at 1.1kg/kw hr
			* 365 // cents to dollars
		);

		// total
		var evo_co2_emissions = high_evo_co2_emissions + low_evo_co2_emissions;
		$( '#evo_annual_co2_emissions' ).html( Math.round( evo_co2_emissions ) );
	},

	calculate_emission_savings : function() {

		// check required fields
		if ( $( '.step1 input, .step4 input' ).hasClass( 'error' ) ) {
			return;
		}

		var annual_emission_saving = $( '#annual_co2_emissions' ).html() - $( '#evo_annual_co2_emissions' ).html();
		$( '#annual_emission_saving' ).html( Math.round( annual_emission_saving ) );

		var annual_emission_saving_balloons = annual_emission_saving / 0.05;
		$( '#annual_emission_saving_balloons' ).html( Math.round( annual_emission_saving_balloons ) );
	},

	graph_running_cost : function () {

		// check required fields
		if ( $( '.step1 input, .step4 input' ).hasClass( 'error' ) ) {
			return;
		}

		function get_running_cost( monthly_power_cost, daily_hours ) {

			var running_cost = (
				monthly_power_cost * ( daily_hours / ( hours_summer + hours_winter ) )
			);
			return running_cost;
		}

		var monthly_power_cost = $( '#annual_power_cost' ).html() / 12;
		var evo_monthly_power_cost = $( '#evo_annual_power_cost' ).html() / 12;
		var hours_summer = $( '#hours_summer' ).val() * 1;
		var hours_winter = $( '#hours_winter' ).val() * 1;

		var data = [
			{label: 'Conventional pump', color: '#ff6666', data:[
				[1.25,get_running_cost( monthly_power_cost, hours_summer )],
				[2.25,get_running_cost( monthly_power_cost, hours_summer )],
				[3.25,get_running_cost( monthly_power_cost, hours_summer )],
				[4.25,get_running_cost( monthly_power_cost, hours_winter )],
				[5.25,get_running_cost( monthly_power_cost, hours_winter )],
				[6.25,get_running_cost( monthly_power_cost, hours_winter )],
				[7.25,get_running_cost( monthly_power_cost, hours_winter )],
				[8.25,get_running_cost( monthly_power_cost, hours_winter )],
				[9.25,get_running_cost( monthly_power_cost, hours_summer )],
				[10.25,get_running_cost( monthly_power_cost, hours_summer )],
				[11.25,get_running_cost( monthly_power_cost, hours_summer )],
				[12.25,get_running_cost( monthly_power_cost, hours_summer )]
			]},
			{label: $( '#pump_model option:selected' ).text() + ' Pump', color: '#77933c', data:[
				[1.5,get_running_cost( evo_monthly_power_cost, hours_summer )],
				[2.5,get_running_cost( evo_monthly_power_cost, hours_summer )],
				[3.5,get_running_cost( evo_monthly_power_cost, hours_summer )],
				[4.5,get_running_cost( evo_monthly_power_cost, hours_winter )],
				[5.5,get_running_cost( evo_monthly_power_cost, hours_winter )],
				[6.5,get_running_cost( evo_monthly_power_cost, hours_winter )],
				[7.5,get_running_cost( evo_monthly_power_cost, hours_winter )],
				[8.5,get_running_cost( evo_monthly_power_cost, hours_winter )],
				[9.5,get_running_cost( evo_monthly_power_cost, hours_summer )],
				[10.5,get_running_cost( evo_monthly_power_cost, hours_summer )],
				[11.5,get_running_cost( evo_monthly_power_cost, hours_summer )],
				[12.5,get_running_cost( evo_monthly_power_cost, hours_summer )]
			]}
		];

		var options = {
			lines: { show: false },
			bars: { show: true, barWidth: 0.25 },
			xaxis: {
				tickDecimals: 0,
				tickSize: 1,
				ticks: [[1.5,'Jan'],[2.5,'Feb'],[3.5,'Mar'],[4.5,'Apr'],[5.5,'May'],[6.5,'Jun'],[7.5,'Jul'],[8.5,'Aug'],[9.5,'Sep'],[10.5,'Oct'],[11.5,'Nov'],[12.5,'Dec']]
			},
			yaxis: {
				tickDecimals: 0,
				tickSize: 5,
				tickFormatter : function( val, axis ) {
					return '$' + val.toFixed( 2 );
				}
			}
		};

		$.plot( $( '#graph_running_cost' ), data, options );
	}
};

$( document ).ready( cost_saving_calculator.init );
