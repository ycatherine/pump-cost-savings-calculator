/**
*	Heat Pump Sizing calculator - SB models.
*	Unfortunately the Excel document this was ported from has some magic numbers (i.e. their 
*	purpose or meaning was unknown), so they have been copied as is.
*	@author Chris Botman <chris@botman.com.au> on behalf of Wordplay Media
*/

var heat_pump_sizing_sb_calculator = {

	init : function() {
	
		// init validation and triggers
		$( '#heat-pump-sizing-sb' ).validate();
		$( 'input, select' ).change( function() {
			heat_pump_sizing_sb_calculator.update();
		});
		
		// all fields are preset, so perform first calculation
		heat_pump_sizing_sb_calculator.update();
	},
	
	update : function () {
	
		this.calculate_kw_required();
		this.calculate_recommended_model();
		this.calculate_costs();
		this.calculate_ave_temps();
	},
	
	calculate_kw_required : function() {

		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var total_meters = $( '#average_length' ).val() * $( '#average_width' ).val() * 
			$( '#average_depth' ).val();
		var desired_temp = $( '#desired_temp' ).val();
		var average_temp = $( '#location_ave_temp' ).val();
		
		var kw_required = (
			total_meters
			* 1.16 // Magic number...
			* 1.3 // Magic number...
			* ( desired_temp - average_temp )
		);
		$( '#kw_required' ).html( kw_required.toFixed( 4 ) );
	},
	
	calculate_recommended_model : function() {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		var recommended_model = 'Contact your local AstralPool office';
		var kw_required = $( '#kw_required' ).html() / 32; // Magic number
		
		if ( $( '#power_phase' ).val() == 'Single Phase' ) {
			
			if ( this.output_condition_1['BM 600'] > kw_required ) {
				recommended_model = 'BM 600';
			}
			else if ( this.output_condition_1['SBM 700'] > kw_required ) {
				recommended_model = 'SBM 700';
			}
			else if ( this.output_condition_1['SBM 800'] > kw_required ) {
				recommended_model = 'SBM 800';
			}
		}
		else { // Three Phase
		
			if ( this.output_condition_1['SBT 700'] > kw_required ) {
				recommended_model = 'SBT 700';
			}
			else if ( this.output_condition_1['SBT 800'] > kw_required ) {
				recommended_model = 'SBT 800';
			}
			else if ( this.output_condition_1['SBT 900'] > kw_required ) {
				recommended_model = 'SBT 900';
			}
		}
		
		$( '#recommended_model' ).html( recommended_model );
	},
	
	calculate_costs : function() {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}
		
		var kw_required = $( '#kw_required' ).html();
		var power_cost = $( '#power_cost' ).val();
		
		var heat_up_cost = (
			kw_required 
			* power_cost
			/ 100
			/ 5
		);
		$( '#heat_up_cost' ).html( heat_up_cost.toFixed( 2 ) );

		var average_length = $( '#average_length' ).val();
		var average_width = $( '#average_width' ).val();
		// has depth been left out on the Excel sheet on purpose?
		
		var daily_cost = (
			average_length
			* average_width
			* 1.16 // Magic number
			* 3
			* power_cost
			/ 100
			/ 5
		);
		$( '#daily_cost' ).html( daily_cost.toFixed( 2 ) );
	},

	output_condition_1 : { 
	//  model 		kW output at 27 degree air temperature, 63% RH & 27 degree entering water temperature
		"BM 600"	: 14,
		"SBM 700" 	: 19,
		"SBM 800"	: 25,
		"SBT 700"	: 19,
		"SBT 800"	: 25,
		"SBT 900"	: 30,
	},
	
	output_condition_2 : { 
	//  model 		kW output at 10 degree air temperature, 63% RH & 27 degree entering water temperature
		"BM 600"	: 10,
		"SBM 700" 	: 14,
		"SBM 800"	: 18,
		"SBT 700"	: 14,
		"SBT 800"	: 18,
		"SBT 900"	: 22,
	},
	
	calculate_ave_temps : function () {
	
		// check required fields
		if ( $( '.step2 input, .step3 input' ).hasClass( 'error' ) ) {
			return;
		}

		var recommended_model = $( '#recommended_model' ).html();
		var total_meters = $( '#average_length' ).val() * $( '#average_width' ).val() * 
			$( '#average_depth' ).val();

		// ensure there is a recommended pump available for each scenario
		
		var pump_kw_output_condition_1 = this.output_condition_1["" + recommended_model];
		if ( pump_kw_output_condition_1 == undefined ) {
			$( '#ave_temp_1' ).html( 'N/A' );
		}
		else {
			var ave_temp_1 = (
				1
				/ 
				( 
					total_meters 
					* 1.16 // Magic numbers
					/ pump_kw_output_condition_1
					* 1.3 
				)
			);
			$( '#ave_temp_1' ).html( ave_temp_1.toFixed( 2 ) );
		}

		var pump_kw_output_condition_2 = this.output_condition_2["" + recommended_model];
		if ( pump_kw_output_condition_2 == undefined ) {
			$( '#ave_temp_2' ).html( 'N/A' );
		}
		else {
			var ave_temp_2 = (
				1
				/ 
				( 
					total_meters 
					* 1.16 // Magic numbers
					/ pump_kw_output_condition_2
					* 1.3 
				)
			);
			$( '#ave_temp_2' ).html( ave_temp_2.toFixed( 2 ) );
		}

	}
};

$( document ).ready( heat_pump_sizing_sb_calculator.init );
